# Setup 

create a dirsctory modified by Eman for the development, here you need to create a folder 

` mkdir odoo_training `

` cd odoo_training `

## 1- Create Odoo server 
Here we are using docker image, you need to make sure you have a basic docker info.

 1.1 Clone the Odoo addons, run this command:
 ``` git clone https://gitlab.com/eman.saeed/odoo-training.git ```

 1.2 Create a custom odoo.conf file

 ` touch odoo.conf `

 and edit the file with this content:

```
 [options]
addons_path = /odoo_addons
data_dir = /var/lib/odoo
admin_passwd = 12345
limit_time_cpu = 1200
limit_time_real = 1800 
```


 1.3 setup docker-compose.yml file
    Run this comman:

     touch docker-compose.yml 

Edit the file with this content:

``` 
version: "3.7"
services:
  db:
    image: postgres:15.5
    ports:
      - "5432:5432"
    environment:
      - POSTGRES_DB=postgres
      - POSTGRES_USER=odoo
      - POSTGRES_PASSWORD=odoo
    volumes:
      - odoo17-db1-data:/var/lib/postgresql/data 
    
  odoo_web:
    image: odoo:17
   
    ports:
      - "8069:8069"
    volumes:
      - ./odoo_addons:/odoo_addons
      - ./odoo.conf:/etc/odoo/odoo.conf
      - odoo17-web-data:/var/lib/odoo
    #command:
      #-d db_name 
      #--update all
      

volumes:
  odoo17-web-data:
  odoo17-db1-data:


```
3- Create a folder for custom addons
  ` mkdir odoo_addons `

4- Run to install docker 

` sudo snap install docker `


5- Run the Odoo server 

` sudo docker-compose up `

Install Odoo with a server in local machine 

1. download the source zip file from this link :
  https://github.com/odoo/odoo

2. Unzip the file, and open the terminal inside the directory
3. install all odoo requirement: sudo pip install -r requirements.txt
4. install npm : sudo apt install npm
5. install rtlcss: sudo npm install -g rtlcss
6. install postgresql: sudo apt install postgresql -y
7. create a user and role called root: 
  1. su postgres 
  2. createuser root -s
  3. go to psql:    psql
  4. add super user to role: alter user root with password '123';
  5. verify installation and run odoo by your user using:
    sudo ./odoo-bin --addons-path=/home/eman/Downloads/odoo17-add/addons,/home/eman/Downloads/custom -d mydb